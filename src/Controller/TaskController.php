<?php

namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @Route("/api", name="app_people_search_all", methods={"GET"})
 */

class TaskController extends AbstractController
{
    /**
     * @Route("/searchAll", name="app_people_search_all", methods={"GET"})
     */
    public function searchAll(): Response
    {
        $data = $this->getDoctrine()
            ->getRepository(Task::class)
            ->findAll();
        $response = [];

        foreach ($data as $record) {
            $response[] = [
                'id' => $record->getId(),
                'name' => $record->getFirstName(),
                'time' => $record->getLastName(),
                'description' => $record->getAge()
            ];
        }

        return $this->json($response);
    }
}